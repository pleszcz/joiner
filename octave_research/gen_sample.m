function y = gen_sample(fstr, n)
  y = [];
  f = inline(fstr);
  for i = 1:n
    y = [y f()];
  end
end

