function test_all(iterations)
  x0 = [15.8*ones(8, 1); 1.0];
  for test_nr = 1:9
    fun_name = strcat("fun", num2str(test_nr));
    [x, fval, info] = fsolve(fun_name, x0, optimset("MaxIter", iterations));
    fmax = max(abs(fval));
    disp(cstrcat(fun_name, ": ", num2str(info), ", ", num2str(fmax)));
  end
endfunction

