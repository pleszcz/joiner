function test_all_norm_mc(mc_iters, solve_iters)
  for test_nr = 1:9
    fun_name = strcat("fun", num2str(test_nr), "(x)");
    disp(cstrcat("[ ", fun_name, " ]"));
    fflush(stdout);
    [found, x, fmax, info, last_it, output, x0] = brute_norm(inline(fun_name), 10000, 8, mc_iters, solve_iters, 1e-6);
    if (found)
      disp(cstrcat("found (info=", num2str(info),
                   ", fmax=", num2str(fmax),
                   ", last_it=", num2str(last_it), ")"));
      disp(output);
      disp(cstrcat("x0: [", num2str(transpose(x0)), "]"));
      disp(cstrcat("x: [", num2str(transpose(x)), "]"));
    else
      disp("not found");
    end
  end
endfunction

