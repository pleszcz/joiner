function y = fnorm(x, K)
  y = 0.0;
  for i = 1:K
    y += x(i) * x(i);
  end
endfunction

