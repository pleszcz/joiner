function [found, x, fmax, info, last_mc_iter, output, x0] = brute_norm(fun, k, dim, mc_iters, solve_iters, eps)
  found = false;
  root = k ^ (1/dim);
  last_mc_iter = -1;

  function x00 = gen_x0()
    x00 = [];
    for i = 1:dim
      x00 = [x00; unifrnd(1, 5 * root)];
    end
    x00 = [x00; 0.1];
  end

  for iter = 1:mc_iters
    x0 = gen_x0();
    [x, fmax, info, output] = fminunc(@(z) norm(fun(z), dim), x0, optimset("MaxIter", solve_iters));
    if (info == 1.0 || ((info == 2.0 || info == 3.0) && fmax < eps))
      found = true;
      last_mc_iter = iter;
      return;
    end
  end
endfunction




