#!/bin/bash

if [ ! \( -d "./output" \) ]; then
  mkdir "./output"
fi

pdflatex -output-directory ./output -halt-on-error praca.tex

