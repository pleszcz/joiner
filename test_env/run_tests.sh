#!/bin/bash

package="$JOINER_JARS/joiner-1.0-jar-with-dependencies.jar"
if [ "$1" -a \( "${1:(-3)}" = "jar" \) ]; then
  package="$1"
  shift 1
fi

logging=
octave_logging=
skip_hadoop=

if [ "$LOGGING" = "TRUE" ]; then
  logging="--main_logging --structure_logging --hadoop_logging --reducers_logging"
fi

if [ "$OCTAVE_LOGGING" = "TRUE" ]; then
  octave_logging="--octave_logging"
fi

if [ "$SKIP_HADOOP" = "TRUE" ]; then
  skip_hadoop="--skip_hadoop"
fi

function run_test() {
  test_name=$1
  
  echo "Running $test_name"  
  
  if [ "$SKIP_HADOOP" != "TRUE" ]; then
    echo "   Uploading input files"
    if [ "`hadoop fs -ls | grep $test_name`" ]; then
      $HADOOP/hadoop fs -rmr $test_name
    fi
    $HADOOP/hadoop fs -mkdir $test_name
    for inp in $test_name/*.in; do
      echo "      Uploading $inp"
      bname=`basename $inp | cut -d. -f1`
      $HADOOP/hadoop fs -mkdir $test_name/$bname
      $HADOOP/hadoop fs -put $inp $test_name/$bname/
    done
  fi

  shares_output="$test_name/shares"
  save_shares_flag=
  load_shares_flag=
  if [ \( "$FORCE_SHARE_RECOMPUTE" = "TRUE" \) -o ! \( -f "$shares_output" \) ]; then
    save_shares_flag="--save_shares_file $shares_output"
  else
    load_shares_flag="--load_shares_file $shares_output"
  fi

  hadoop_jar_cmd="\
$HADOOP/hadoop jar $package $logging $octave_logging $skip_hadoop \
$save_shares_flag $load_shares_flag $test_name/config"
  echo "   Running task ($hadoop_jar_cmd)"
  $hadoop_jar_cmd
  
  if [ "$SKIP_HADOOP" != "TRUE" ]; then
    echo "   Checking output"
    if [ -f "$test_name/out" ]; then
      rm "$test_name/out"
    fi
    $HADOOP/hadoop fs -getmerge $test_name/out $test_name/out
    sort $test_name/out | uniq | \
      diff - $test_name/correct_out 2> /dev/null > /dev/null
    if [ $? -eq 0 ]; then
      echo -e "\e[32m   OUTPUT OK!\e[0m"
    else
      echo -e "\e[31m   OUTPUT INCORRECT!\e[0m"
    fi
    echo
  fi
}

if [ ! \( "$package" \) ]; then
  echo "Usage: $0 [<jar>] [<test_name>]"
  exit 1
fi

if [ "$#" -gt "0" ]; then
  while [ "$#" -gt "0" ]; do
    run_test "$1"
    shift 1
    echo
    echo
  done
else
  for i in test*; do
    run_test $i
    echo
    echo
  done
fi

