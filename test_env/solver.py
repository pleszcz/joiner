#!/usr/bin/python

# To jest tylko podstawa do powazniejszych rzeczy.
# Wlasciwie nic tu jeszcze nie ma.

import sys
import sets
import argparse

def readPairs(file_name):
  fd = open(file_name, "r")
  lines = fd.read().splitlines()
  fd.close()
  return map(lambda x: tuple(map(int, x.split(" "))), lines)

def makeGraph(pairs):
  def addToDict(d, key, value):
    values = d.get(key)
    if values is None:
      d[key] = [value]
    else:
      values.append(value)

  graph = {}
  for (x, y) in pairs:
    addToDict(graph, x, y)
  return graph

def parseArgs():
  parser = argparse.ArgumentParser()
  parser.add_argument("-c", "--config", default="./config")
  return parser.parse_args()

def readConfigFile(fileName):
  fd = open(fileName, "r")
  lines = map(lambda s: s.split(), fd.readlines())
  fd.close()
  return [("./"+v[0]+".in", v[2:]) for v in lines if len(v) >= 4]

def main():
  args = parseArgs()
  filesStructure = readConfigFile(args.config)
  print filesStructure


if __name__ == "__main__":
  main()

