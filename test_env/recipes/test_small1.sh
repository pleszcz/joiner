#!/bin/bash

if [ -d "test_small1" ]; then
  rm -rf test_small1
fi
mkdir test_small1

if [ -d "opt_test_small1" ]; then
  rm -rf opt_test_small1
fi
mkdir opt_test_small1

~/test_scripts/research/tests_generator.py 120 100 150 300 350 > test_small1/relR1.in
~/test_scripts/research/tests_generator.py 300 300 350 500 550 > test_small1/relR2.in
~/test_scripts/research/tests_generator.py 80  100 150 500 550 > test_small1/relR3.in

cat > opt_test_small1/config <<END
300 test_small1/out
test_small1/relR1 120 A B
test_small1/relR2 300 B C
test_small1/relR3 80  A C
END

cat > test_small1/config <<END
8 test_small1/out
test_small1/relR1 120 A B
test_small1/relR2 300 B C
test_small1/relR3 80  A C
END

echo -n "Output lines: "
~/test_scripts/research/3solver.py \
test_small1/relR1.in \
test_small1/relR2.in \
test_small1/relR3.in | sort | uniq | tee test_small1/correct_out | wc -l

