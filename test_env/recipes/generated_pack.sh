#!/bin/bash

for structure in cycle triangles chain; do
  for i in 4 5 6 7 8; do
    test_name="test_$structure$i"
    if [ -d $test_name ]; then
      rm -rf $test_name
    fi
    mkdir $test_name
    cd $test_name
    ~/test_scripts/generator.py $structure \
      -c $i \
      --testName $test_name \
      --minRelationSize 1000 \
      --maxRelationSize 2000 \
      --maxOutputSize 100
    cd ..
  done
done

