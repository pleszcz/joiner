#!/usr/bin/python

import os
import sys
import random

def create_column(size, f, t):
  return [random.choice(xrange(f, t+1)) for i in xrange(size)]

args = map(int, sys.argv[1:])

col1 = create_column(*args[:3])
col2 = create_column(args[0], *args[3:5])

for p in zip(col1, col2):
  print "%d %d" % p


