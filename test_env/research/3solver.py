#!/usr/bin/python

import sys
import sets

def ReadPairs(file_name):
  fd = open(file_name, "r")
  lines = fd.read().splitlines()
  fd.close()
  return map(lambda x: tuple(map(int, x.split(" "))), lines)

def MakeGraph(pairs):
  def AddToDict(d, key, value):
    values = d.get(key)
    if values is None:
      d[key] = [value]
    else:
      values.append(value)

  graph = {}
  for (x, y) in pairs:
    AddToDict(graph, x, y)
  return graph

def Main(r, s, t):
  rPairs = ReadPairs(r)   
  sPairs = ReadPairs(s)   
  tPairs = ReadPairs(t)   

  sGraph = MakeGraph(sPairs)
  rPairs.sort()
  tSet = sets.Set(tPairs)

  for (a, b) in rPairs:
    cAdjs = sGraph.get(b)
    if cAdjs is not None:
      for c in cAdjs:
        if (a, c) in tSet:
          print "%d %d %d" % (a,b,c)

if __name__ == "__main__":
  Main(*sys.argv[1:])
