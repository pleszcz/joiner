#!/usr/bin/python

import sys
import sets

def ReadPairs(file_name):
  fd = open(file_name, "r")
  lines = fd.read().splitlines()
  fd.close()
  return map(lambda x: tuple(map(int, x.split(" "))), lines)

def MakeGraph(pairs):
  def AddToDict(d, key, value):
    values = d.get(key)
    if values is None:
      d[key] = [value]
    else:
      values.append(value)

  graph = {}
  for (x, y) in pairs:
    AddToDict(graph, x, y)
  return graph

def Main(r, s, t):
  rPairs = ReadPairs(r)   
  sPairs = ReadPairs(s)   
  tPairs = ReadPairs(t)   

  rPairs.sort()
  sGraph = MakeGraph(sPairs)
  tGraph = MakeGraph(tPairs)

  for (a, b) in rPairs:
    cAdjs = sGraph.get(a)
    dAdjs = tGraph.get(a)
    if (cAdjs is not None) and (dAdjs is not None):
      for c in cAdjs:
        for d in dAdjs:
          print "%d %d %d %d" % (a, b, c, d)

if __name__ == "__main__":
  Main(*sys.argv[1:])
