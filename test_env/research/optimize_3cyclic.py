#!/usr/bin/python

import sys
import math

def main(r, s, t, k):
  def elapsed_cost(a, b, c):
    return (r*c + s*a + t*b) / float(a*b*c)

  a = (r*t*k / float(s*s)) ** (1./3.)
  b = (r*s*k / float(t*t)) ** (1./3.)
  c = (s*t*k / float(r*r)) ** (1./3.)
  print "a = " + str(a)
  print "b = " + str(b)
  print "c = " + str(c)
  print "abc = " + str(a*b*c)

  costs = []

  for x in [int(math.floor(a)), int(math.ceil(a))]:
    for y in [int(math.floor(b)), int(math.ceil(b))]:
      for z in [int(math.floor(c)), int(math.ceil(c))]:
        if x*y*z <= k:
          costs.append((elapsed_cost(x, y, z), x, y, z, x*y*z))
  costs.sort()
  for v in costs:
    print "%f  =  elapsed_cost(%d, %d, %d) [%d]" % v

if __name__ == '__main__':
  args = map(int, sys.argv[1:])
  main(*args)
