#!/usr/bin/python

import sys
import argparse
from random import randint, shuffle, sample
from sets import Set

COMMANDS = [ "cycle", "triangles", "chain", "random" ]

GOOD_VALUE_MASK = "good_%d_%d"
BAD_VALUE_MASK = "bad_%d_%d"

def columnFromNumber(x):
  return chr(ord('A') + x)

def numberFromColumn(x):
  return ord(x) - ord('A')

def filenameFromColumn(c):
  return "relR%d.in" % (c + 1)

class StructureGenerator:
  def __init__(self, columnsNumber, relationsNumber, minAttributes, maxAttributes):
    self.columnsNumber = columnsNumber
    self.relationsNumber = relationsNumber
    self.minAttributes = minAttributes
    self.maxAttributes = maxAttributes

  def cycle(self):
    if self.columnsNumber < 3:
      return None
    result = []
    for i in range(self.columnsNumber - 1):
      result.append( (columnFromNumber(i), columnFromNumber(i + 1)) )
    result.append( ('A', columnFromNumber(self.columnsNumber - 1)) )
    return result

  def triangles(self):
    if self.columnsNumber < 3:
      return None
    result = [ ('A', 'B') ]
    for i in range(2, self.columnsNumber):
      result.append( (columnFromNumber(i - 2), columnFromNumber(i)) )
      result.append( (columnFromNumber(i - 1), columnFromNumber(i)) )
    return result

  def chain(self):
    if self.columnsNumber < 3:
      return None
    result = []
    for i in range(self.columnsNumber - 1):
      result.append( (columnFromNumber(i), columnFromNumber(i + 1)) )
    return result

  def random(self):
    attributesInEachStep = sample(xrange(2, self.columnsNumber - 1), self.relationsNumber - 1)
    attributesInEachStep.sort()
    attributesInEachStep.append(self.columnsNumber)
    for i in range(self.relationsNumber - 1, 0, -1):
      attributesInEachStep[i] -= attributesInEachStep[i - 1]

    remainedColumns = Set(range(0, self.columnsNumber))
    integerResult = [sample(remainedColumns, attributesInEachStep[0])]
    usedColumns = Set(integerResult[0])
    remainedColumns -= usedColumns

    for i in range(1, self.relationsNumber):
      oldColumns = sample(usedColumns, min(len(usedColumns), max(1, self.minAttributes - attributesInEachStep[i])))
      currentNewColumns = sample(remainedColumns, attributesInEachStep[i])
      newTuple = oldColumns + currentNewColumns
      shuffle(newTuple)
      integerResult.append(newTuple)
      currentNewColumnsSet = Set(currentNewColumns)
      usedColumns = usedColumns.union(currentNewColumnsSet)
      remainedColumns -= currentNewColumnsSet

    return map(lambda x: tuple(map(columnFromNumber, x)), integerResult)

class TuplesGenerator:
  def __init__(self, columnsNumber, structure, minRelationSize, maxRelationSize, maxOutputSize, integerFormat):
    self.columnsNumber = columnsNumber
    self.structure = structure
    self.relationsNumber = len(self.structure)
    self.minRelationSize = minRelationSize
    self.maxRelationSize = maxRelationSize
    self.maxOutputSize = maxOutputSize
    self.integerFormat = integerFormat
    self.integerBase = 10
    if self.integerFormat:
      while self.integerBase <= self.maxRelationSize:
        self.integerBase *= 10

  def initStuff(self):
    self.relationsSizes = [randint(self.minRelationSize, self.maxRelationSize) for i in range(self.relationsNumber)]
    self.columnsCounters = [1] * self.columnsNumber
    self.relationsContainers = [[] for i in range(self.relationsNumber)]
    self.outTuples = []

  def makeTuple(self, isGood, colNr, tNr):
    if self.integerFormat:
      result = (self.integerBase * (colNr + 1) + tNr) * 100
      if isGood:
        result += 1
      else:
        result += 2
      return str(result)
    else:
      if isGood:
        return GOOD_VALUE_MASK % (colNr, tNr)
      else:
        return BAD_VALUE_MASK % (colNr, tNr)

  def addOutTuple(self):
    values = []
    for col in range(self.columnsNumber):
      values.append(self.makeTuple(True, col, self.columnsCounters[col]))
    self.outTuples.append(values)
    maxNotReached = True
    for relInd in range(self.relationsNumber):
      currentColumns = self.structure[relInd]
      currentValues = [values[numberFromColumn(c)] for c in currentColumns]
      self.relationsContainers[relInd].append(currentValues)
      if len(self.relationsContainers[relInd]) >= self.relationsSizes[relInd]:
        maxNotReached = False
    for col in range(self.columnsNumber):
      self.columnsCounters[col] += 1
    return maxNotReached

  def createBadTuple(self, relInd):
    result = []
    for col in self.structure[relInd]:
      colNr = numberFromColumn(col)
      result.append(self.makeTuple(False, colNr, self.columnsCounters[colNr]))
      self.columnsCounters[colNr] += 1
    return result

  def addRemainingTuples(self, relInd):
    while len(self.relationsContainers[relInd]) < self.relationsSizes[relInd]:
      self.relationsContainers[relInd].append(self.createBadTuple(relInd))

  def generate(self):
    self.initStuff()
    for i in range(self.maxOutputSize):
      if not self.addOutTuple():
        break
    for relInd in range(self.relationsNumber):
      self.addRemainingTuples(relInd)
    for vals in self.relationsContainers:
      shuffle(vals)

  def uniqueList(self, l):
    if len(l) == 0:
      return l
    outl = [l[0]]
    for x in l:
      if x != outl[-1]:
        outl.append(x)
    return outl

  def writeToDisk(self):
    # inputs
    for relInd in range(self.relationsNumber):
      relation = self.relationsContainers[relInd]
      fileName = filenameFromColumn(relInd)
      fd = open(fileName, "w")
      lines = map(lambda x: " ".join(x), relation)
      fd.write("\n".join(lines))
      fd.write("\n")
      fd.close()
    # output
    fd = open("correct_out", "w")
    lines = map(lambda x: " ".join(x), self.outTuples)
    lines.sort()
    fd.write("\n".join(self.uniqueList(lines)))
    fd.write("\n")
    fd.close()

def prepareConfigFile(testName, structure, content, reducersNumber):
  fd = open("config", "w")
  lines = ["{} {}/out".format(reducersNumber, testName)]
  for ind in range(len(structure)):
    cols = structure[ind]
    tups = content[ind]
    lines.append("{}/relR{} {} {}".format(testName, ind+1, len(tups), " ".join(cols)))
  fd.write("\n".join(lines))
  fd.write("\n")
  fd.close()

def parseArgs():
  parser = argparse.ArgumentParser()
  parser.add_argument("command", choices=COMMANDS)
  parser.add_argument("--minAttributes", type=int, default=2)
  parser.add_argument("--maxAttributes", type=int, default=4)
  parser.add_argument("--minRelationSize", type=int, default=1000)
  parser.add_argument("--maxRelationSize", type=int, default=1000)
  parser.add_argument("--maxOutputSize", type=int, default=1000)
  parser.add_argument("-c", "--columns", type=int, default=3)
  parser.add_argument("-r", "--relations", type=int, default=3)
  parser.add_argument("--reducers", type=int, default=8)
  parser.add_argument("--testName", default="test")
  parser.add_argument("-s", "--nointegers_in_input", default=False, action="store_true")
  return parser.parse_args()

def main():
  args = parseArgs()
  print "Args:"
  print args
  sg = StructureGenerator(args.columns, args.relations, args.minAttributes, args.maxAttributes)
  structure = getattr(sg, args.command)()
  if structure is None:
    print "Error while generating structure"
    sys.exit(1)
  print "Structure:"
  print structure
  tg = TuplesGenerator(args.columns, structure,
      args.minRelationSize, args.maxRelationSize, args.maxOutputSize,
      not args.nointegers_in_input)
  tg.generate()
  tg.writeToDisk()
  prepareConfigFile(args.testName, structure, tg.relationsContainers, args.reducers)

if __name__ == "__main__":
  main()
