package joiner.hadoop;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

import java.io.IOException;
import java.util.Iterator;

public class JoinReducer extends ConfiguredMapReduceBase implements
        Reducer<IntWritable, TransferObject, NullWritable, Text> {

    private boolean logging = false;

    @Override
    public void configure(JobConf job) {
        super.configure(job);
        logging = Boolean.parseBoolean(job.get(JoinTask.REDUCERS_LOGGING_FLAG));
    }

    private void logHash(int hash) {
        if (logging)
            System.out.println("HASH: " + hash);
    }

    private void logObject(TransferObject tObject) {
        if (logging)
            System.out.println(tObject.sourceFile + " [ "
                    + StringUtils.join(tObject.values, ", ") + " ]");
    }

    @Override
    public void reduce(IntWritable key, Iterator<TransferObject> sourceValues,
                       OutputCollector<NullWritable, Text> emiter, Reporter reporter)
            throws IOException {
        logHash(key.get());
        TuplesBuilder tuplesBuilder = new TuplesBuilder(joinTaskConf);
        while (sourceValues.hasNext()) {
            TransferObject tObject = sourceValues.next();
            logObject(tObject);
            tuplesBuilder.addTuple(tObject);
        }
        tuplesBuilder.emit(emiter);
    }
}
