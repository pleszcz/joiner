package joiner.hadoop;

import joiner.main.Main;
import joiner.stuff.MyLogger;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.util.*;

public class JoinTask extends Configured implements Tool {
    private static MyLogger logger = new MyLogger(JoinTask.class);

    public static final String JOIN_CONFIG_FILE_FLAG = "JOIN_CONFIG_FILE";
    public static final String REDUCERS_LOGGING_FLAG = "REDUCERS_LOGGING";

    private String[] commandLineArgs;
    private String outputFile;
    private int reducersNumber;
    private Map<String, Integer[]> sources;
    private Integer[] sharesSizes;

    private String configFile;

    public void setCommandLineArgs(String[] commandLineArgs) {
        this.commandLineArgs = commandLineArgs;
    }

    public void setOutputFile(String outputFile) {
        this.outputFile = outputFile;
    }

    public void setReducersNumber(int reducersNumber) {
        this.reducersNumber = reducersNumber;
    }

    public void setSources(Map<String, Integer[]> sources) {
        this.sources = sources;
    }

    public void setSharesSizes(Integer[] sharesSizes) {
        this.sharesSizes = sharesSizes;
    }

    public JoinTask() {
        super();
    }

    private void writeTaskConfiguration(JobConf jobConf) throws Exception {
        TaskConfiguration taskConf = new TaskConfiguration();
        taskConf.sharesSizes = sharesSizes;
        taskConf.setSources(sources);
        configFile = "task-config-" + (new Random()).nextInt();
        taskConf.toFile(getConf(), configFile);
        jobConf.set(JOIN_CONFIG_FILE_FLAG, configFile);
    }

    public int run(String[] args) throws Exception {
        JobConf jobConf = new JobConf(getConf(), JoinTask.class);
        jobConf.setJobName("join");

        jobConf.setMapperClass(JoinMapper.class);
        jobConf.setReducerClass(JoinReducer.class);

        jobConf.setNumReduceTasks(reducersNumber);

        List<Path> inputPaths = new ArrayList<>();
        for (String fileName : sources.keySet())
            inputPaths.add(new Path(fileName));
        FileInputFormat.setInputPaths(jobConf, inputPaths.toArray(new Path[inputPaths.size()]));
        FileOutputFormat.setOutputPath(jobConf, new Path(outputFile));

        jobConf.setMapOutputKeyClass(IntWritable.class);
        jobConf.setMapOutputValueClass(TransferObject.class);
        jobConf.setOutputFormat(TextOutputFormat.class);
        jobConf.setOutputKeyClass(NullWritable.class);
        jobConf.setOutputValueClass(Text.class);

        writeTaskConfiguration(jobConf);

        jobConf.set(REDUCERS_LOGGING_FLAG, Boolean.toString(Main.REDUCERS_LOGGING));

        Date startTime = new Date();
        logger.log("Job started: " + startTime);
        JobClient.runJob(jobConf);
        Date end_time = new Date();
        logger.log("Job ended: " + end_time);
        logger.log("The job took "
                + (end_time.getTime() - startTime.getTime()) / 1000
                + " seconds.");

        FileSystem.get(getConf()).delete(new Path(configFile), true);

        return 0;
    }

    public void launch() throws Exception {
        int res = ToolRunner.run(new Configuration(), this, commandLineArgs);
        System.exit(res);
    }

}
