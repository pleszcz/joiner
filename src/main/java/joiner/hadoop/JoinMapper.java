package joiner.hadoop;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

import java.io.IOException;

public class JoinMapper extends ConfiguredMapReduceBase implements
        Mapper<LongWritable, Text, IntWritable, TransferObject> {

    @Override
    public void map(LongWritable lineNr, Text line,
                    OutputCollector<IntWritable, TransferObject> emiter,
                    Reporter reporter) throws IOException {
        extractSourceName(reporter);
        JoinEmiter joinEmiter = new JoinEmiter(joinTaskConf, sourceName);
        joinEmiter.parseLine(line.toString());
        joinEmiter.emit(emiter);
    }
}
