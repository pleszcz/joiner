package joiner.hadoop;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.OutputCollector;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class TuplesBuilderTester {

    public static class SimpleCollector implements
            OutputCollector<NullWritable, Text> {
        @Override
        public void collect(NullWritable arg0, Text t) throws IOException {
            System.out.println(t.toString());
        }
    }

    private static void test1() throws IOException {
        TaskConfiguration conf = new TaskConfiguration();
        conf.sharesSizes = new Integer[]{3, 3, 3};
        Map<String, Integer[]> columns = new HashMap<>();
        columns.put("R", new Integer[]{0, 1});
        columns.put("S", new Integer[]{1, 2});
        columns.put("T", new Integer[]{0, 2});
        conf.setSources(columns);

        TuplesBuilder builder = new TuplesBuilder(conf);

        builder.addTuple(new TransferObject("R", new Integer[]{1, 11}));
        builder.addTuple(new TransferObject("R", new Integer[]{2, 12}));
        builder.addTuple(new TransferObject("R", new Integer[]{3, 13}));
        builder.addTuple(new TransferObject("R", new Integer[]{5, 19}));

        builder.addTuple(new TransferObject("S", new Integer[]{11, 101}));
        builder.addTuple(new TransferObject("S", new Integer[]{12, 102}));
        builder.addTuple(new TransferObject("S", new Integer[]{13, 103}));
        builder.addTuple(new TransferObject("S", new Integer[]{14, 191}));

        builder.addTuple(new TransferObject("T", new Integer[]{1, 101}));
        builder.addTuple(new TransferObject("T", new Integer[]{2, 102}));
        builder.addTuple(new TransferObject("T", new Integer[]{3, 103}));
        builder.addTuple(new TransferObject("T", new Integer[]{8, 897}));

        builder.emit(new SimpleCollector());
    }

    private static void readAndAddPairs(TuplesBuilder builder, String fileName,
                                        String relName) throws Exception {
        BufferedReader br = new BufferedReader(new FileReader(fileName));
        String line = br.readLine();
        while (line != null) {
            StringTokenizer tokenizer = new StringTokenizer(line);
            if (tokenizer.hasMoreTokens()) {
                List<Integer> list = new ArrayList<>();
                while (tokenizer.hasMoreTokens())
                    list.add(new Integer(tokenizer.nextToken()));
                builder.addTuple(new TransferObject(relName, list
                        .toArray(new Integer[0])));
            }
            line = br.readLine();
        }
    }

    private static void test2(String[] args) throws Exception {
        // System.out.println(StringUtils.join(args, " "));

        TaskConfiguration conf = new TaskConfiguration();
        conf.sharesSizes = new Integer[]{3, 3, 3};
        Map<String, Integer[]> columns = new HashMap<>();
        columns.put("R", new Integer[]{0, 1});
        columns.put("S", new Integer[]{1, 2});
        columns.put("T", new Integer[]{0, 2});
        conf.setSources(columns);

        TuplesBuilder builder = new TuplesBuilder(conf);
        readAndAddPairs(builder, args[0], "R");
        readAndAddPairs(builder, args[1], "S");
        readAndAddPairs(builder, args[2], "T");
        builder.emit(new SimpleCollector());
    }

    private static void test3() throws IOException {
        TaskConfiguration conf = new TaskConfiguration();
        conf.sharesSizes = new Integer[]{3, 3, 3, 3, 3, 3, 3, 3};
        Map<String, Integer[]> columns = new HashMap<>();
        columns.put("R1", new Integer[]{1, 2, 3, 5, 6});
        columns.put("R2", new Integer[]{3, 4, 5});
        columns.put("R3", new Integer[]{0, 1});
        columns.put("R4", new Integer[]{5, 6, 7});
        conf.setSources(columns);

        TuplesBuilder builder = new TuplesBuilder(conf);

        builder.addTuple(new TransferObject("R1",
                new Integer[]{2, 3, 4, 6, 7}));
        builder.addTuple(new TransferObject("R1", new Integer[]{4, 6, 8, 12,
                14}));

        builder.addTuple(new TransferObject("R1", new Integer[]{4, 1, 8, 10,
                12}));
        builder.addTuple(new TransferObject("R1", new Integer[]{4, 6, 8, 1,
                12}));
        builder.addTuple(new TransferObject("R1", new Integer[]{4, 6, 1, 10,
                12}));

        builder.addTuple(new TransferObject("R2", new Integer[]{4, 5, 6}));
        builder.addTuple(new TransferObject("R2", new Integer[]{8, 10, 12}));

        builder.addTuple(new TransferObject("R2", new Integer[]{8, 10, 121}));
        builder.addTuple(new TransferObject("R2", new Integer[]{8, 101, 12}));
        builder.addTuple(new TransferObject("R2", new Integer[]{81, 10, 12}));

        builder.addTuple(new TransferObject("R3", new Integer[]{1, 2}));
        builder.addTuple(new TransferObject("R3", new Integer[]{2, 4}));

        builder.addTuple(new TransferObject("R3", new Integer[]{21, 4}));
        builder.addTuple(new TransferObject("R3", new Integer[]{2, 41}));
        builder.addTuple(new TransferObject("R3", new Integer[]{21, 41}));

        builder.addTuple(new TransferObject("R4", new Integer[]{6, 7, 8}));
        builder.addTuple(new TransferObject("R4", new Integer[]{12, 14, 16}));

        builder.addTuple(new TransferObject("R4", new Integer[]{12, 14, 161}));
        builder.addTuple(new TransferObject("R4", new Integer[]{12, 141, 16}));
        builder.addTuple(new TransferObject("R4",
                new Integer[]{1211, 14, 16}));

        builder.emit(new SimpleCollector());
    }

    public static void main(String[] args) throws Exception {
        // test1();
        // test2(args);
        test3();
    }
}
