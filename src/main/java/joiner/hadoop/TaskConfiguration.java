package joiner.hadoop;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TaskConfiguration implements Serializable {
    private static final long serialVersionUID = -7296883380839113679L;

    public Integer[] sharesSizes;
    private String[] sources;
    private ArrayList<Integer[]> columnsIndices;

    public void setSources(Map<String, Integer[]> map) {
        List<String> sourcesList = new ArrayList<>();
        columnsIndices = new ArrayList<>();
        for (Map.Entry<String, Integer[]> kv : map.entrySet()) {
            sourcesList.add(kv.getKey());
            columnsIndices.add(kv.getValue());
        }
        sources = sourcesList.toArray(new String[0]);
    }

    public Map<String, Integer[]> getSources() {
        Map<String, Integer[]> map = new HashMap<String, Integer[]>();
        for (int i = 0; i < sources.length; i++)
            map.put(sources[i], columnsIndices.get(i));
        return map;
    }

    public static TaskConfiguration fromFile(Configuration conf, String fileName) throws Exception {
        FileSystem fileSystem = FileSystem.get(conf);
        Path path = new Path(fileName);
        FSDataInputStream fileIn = fileSystem.open(path);
        ObjectInputStream in = new ObjectInputStream(fileIn);
        TaskConfiguration joinTaskConf = (TaskConfiguration) in.readObject();
        in.close();
        fileIn.close();
        //fileSystem.close();
        return joinTaskConf;
    }

    public void toFile(Configuration conf, String fileName) throws Exception {
        FileSystem fileSystem = FileSystem.get(conf);
        Path path = new Path(fileName);
        FSDataOutputStream fileOut = fileSystem.create(path);
        ObjectOutputStream out = new ObjectOutputStream(fileOut);
        out.writeObject(this);
        out.close();
        fileOut.close();
        //fileSystem.close();
    }
}
