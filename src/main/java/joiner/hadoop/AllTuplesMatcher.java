package joiner.hadoop;

import java.util.ArrayList;
import java.util.Iterator;

public class AllTuplesMatcher extends TuplesMatcher {

    private ArrayList<Integer[]> content;

    public AllTuplesMatcher(int columnsNumber, Integer[] currentColumns, ArrayList<Integer[]> content) {
        super(columnsNumber, currentColumns);
        this.content = content;
    }

    public class AllTuplesIterator implements Iterator<Integer[]> {
        private int columnsNumber;
        private Integer[] currentColumns;
        private Iterator<Integer[]> internalIterator;

        public AllTuplesIterator(int columnsNumber, Integer[] currentColumns,
                                 Iterator<Integer[]> internalIterator) {
            this.columnsNumber = columnsNumber;
            this.currentColumns = currentColumns;
            this.internalIterator = internalIterator;
        }

        @Override
        public boolean hasNext() {
            return internalIterator.hasNext();
        }

        @Override
        public Integer[] next() {
            Integer[] values = internalIterator.next();
            Integer[] result = new Integer[columnsNumber];
            for (int i = 0; i < currentColumns.length; i++)
                result[currentColumns[i]] = values[i];
            return result;
        }

        @Override
        public void remove() {
        }

    }

    @Override
    public Iterator<Integer[]> iterator() {
        return new AllTuplesIterator(columnsNumber, currentColumns, content.iterator());
    }

    @Override
    public void match(Integer[] pattern) {
    }

    @Override
    public Integer[] getResultColumns() {
        return currentColumns;
    }

}
