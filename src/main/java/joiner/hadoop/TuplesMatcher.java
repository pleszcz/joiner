package joiner.hadoop;

public abstract class TuplesMatcher implements Iterable<Integer[]> {
    protected int columnsNumber;
    protected Integer[] currentColumns;

    public TuplesMatcher(int columnsNumber, Integer[] currentColumns) {
        this.columnsNumber = columnsNumber;
        this.currentColumns = currentColumns;
    }

    public abstract void match(Integer[] pattern);

    public abstract Integer[] getResultColumns();

    public static TuplesMatcher createNextMatcher(TuplesMatcher oldMatcher, Integer[] newColumns) {
        return null;
    }
}
