package joiner.hadoop;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapred.OutputCollector;

import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class JoinEmiter extends ConfiguredForTask {

    private String sourceName;

    private Integer[] currentIndices;
    private Integer[] currentColumnsValues;

    private Integer[] filledColumns;
    private Integer[] indicesForKey;

    private OutputCollector<IntWritable, TransferObject> emiter;

    public JoinEmiter(TaskConfiguration conf, String source) {
        super(conf);
        for (String shortSource : sources.keySet()) {
            if (source.contains(shortSource)) {
                sourceName = shortSource;
                break;
            }
        }
        currentIndices = sources.get(sourceName);

		/*
         * System.out.println("Sources:"); for (Map.Entry<String, Integer[]> it
		 * : sources.entrySet()) { System.out.println(it.getKey() + ": [" +
		 * StringUtils.join(it.getValue(), ", ") + "]"); }
		 * System.out.println("source: " + source);
		 * System.out.println("sourceName: " + sourceName);
		 * System.out.println("sharesSizes: " + StringUtils.join(sharesSizes,
		 * ", "));
		 */
    }

    public void parseLine(String line) {
        StringTokenizer tokenizer = new StringTokenizer(line);
        ArrayList<Integer> values = new ArrayList<Integer>();
        while (tokenizer.hasMoreTokens())
            values.add(new Integer(tokenizer.nextToken()));
        currentColumnsValues = values.toArray(new Integer[values.size()]);
    }

    private void prepare() {
        int allColumnsNumber = sharesSizes.length;

        filledColumns = new Integer[allColumnsNumber];
        for (int i = 0; i < currentIndices.length; i++)
            filledColumns[currentIndices[i]] = currentColumnsValues[i];

        ArrayList<Integer> indicesForKeyList = new ArrayList<>();
        for (int i = 0; i < allColumnsNumber; i++)
            if (sharesSizes[i] > 1)
                indicesForKeyList.add(i);
        indicesForKey = indicesForKeyList.toArray(new Integer[indicesForKeyList.size()]);
    }

    private int hashValue(int value, int mod) {
        return value % mod;
    }

    private void recEmit(int index, int hashBase, int hashAcc)
            throws IOException {
        if (index >= indicesForKey.length) {
            emiter.collect(new IntWritable(hashAcc), new TransferObject(
                    sourceName, currentColumnsValues));
        } else {
            int columnIndex = indicesForKey[index];
            if (filledColumns[columnIndex] == null) {
                for (int value = 0; value < sharesSizes[columnIndex]; value++)
                    recEmit(index + 1, hashBase * sharesSizes[columnIndex],
                            hashAcc + value * hashBase);
            } else {
                int hashedValue = hashValue(filledColumns[columnIndex],
                        sharesSizes[columnIndex]);
                recEmit(index + 1, hashBase * sharesSizes[columnIndex], hashAcc
                        + hashedValue * hashBase);
            }
        }
    }

    public void emit(OutputCollector<IntWritable, TransferObject> emiter)
            throws IOException {
        this.emiter = emiter;
        prepare();
        recEmit(0, 1, 0);
    }
}
