package joiner.hadoop;

import org.apache.hadoop.mapred.FileSplit;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Reporter;

public class ConfiguredMapReduceBase extends MapReduceBase {

    protected TaskConfiguration joinTaskConf;
    protected String sourceName;

    @Override
    public void configure(JobConf job) {
        super.configure(job);
        try {
            joinTaskConf = TaskConfiguration.fromFile(job, job.get(JoinTask.JOIN_CONFIG_FILE_FLAG));
        } catch (Exception e) {
            e.printStackTrace();
            throw (new Error("Cannot read config file."));
        }
    }

    protected void extractSourceName(Reporter reporter) {
        sourceName = ((FileSplit) reporter.getInputSplit()).getPath()
                .toString();
    }
}
