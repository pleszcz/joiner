package joiner.hadoop;

import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.Map;

public class JoinTaskTester {

    public static void main(String[] args) throws Exception {
        System.out.println("args: " + StringUtils.join(args, " "));
        JoinTask task = new JoinTask();
        task.setCommandLineArgs(args);
        task.setOutputFile(args[3]);
        task.setReducersNumber(8);
        task.setSharesSizes(new Integer[]{2, 2, 2});

        Map<String, Integer[]> sources = new HashMap<>();
        sources.put(args[0], new Integer[]{0, 1});
        sources.put(args[1], new Integer[]{1, 2});
        sources.put(args[2], new Integer[]{0, 2});
        task.setSources(sources);

        task.launch();
    }

}
