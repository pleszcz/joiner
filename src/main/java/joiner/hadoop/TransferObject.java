package joiner.hadoop;

import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.EOFException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TransferObject implements Writable {
    public String sourceFile;
    public Integer[] values;

    public TransferObject(String sourceFile, Integer[] values) {
        this.sourceFile = sourceFile;
        this.values = values;
    }

    public TransferObject() {
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        sourceFile = in.readUTF();
        List<Integer> valuesList = new ArrayList<>();
        try {
            while (true) {
                Integer value = in.readInt();
                valuesList.add(value);
            }
        } catch (EOFException e) {
        }
        values = valuesList.toArray(new Integer[valuesList.size()]);
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeUTF(sourceFile);
        for (Integer value : values)
            out.writeInt(value);
    }
}
