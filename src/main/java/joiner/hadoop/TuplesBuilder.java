package joiner.hadoop;

import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.OutputCollector;

import java.io.IOException;
import java.util.*;

public class TuplesBuilder extends ConfiguredForTask {

    private Map<String, ArrayList<Integer[]>> tuples = new HashMap<>();
    private TuplesMatcher[] matchers;
    private OutputCollector<NullWritable, Text> emiter;

    public TuplesBuilder(TaskConfiguration conf) {
        super(conf);
    }

    public void addTuple(TransferObject input) {
        ArrayList<Integer[]> container = tuples.get(input.sourceFile);
        if (container == null) {
            ArrayList<Integer[]> newContainer = new ArrayList<>();
            newContainer.add(input.values);
            tuples.put(input.sourceFile, newContainer);
        } else {
            container.add(input.values);
        }
    }

    private void recEmit(int i, Integer[] acc) throws IOException {
        if (i >= matchers.length) {
            emiter.collect(NullWritable.get(), new Text(StringUtils.join(acc, " ")));
        } else {
            matchers[i].match(acc);
            for (Integer[] newTuple : matchers[i])
                recEmit(i + 1, newTuple);
        }
    }

    public void emit(OutputCollector<NullWritable, Text> emiter) throws IOException {
        this.emiter = emiter;
        Set<String> strings = sources.keySet();
        String[] keys = strings.toArray(new String[strings.size()]);
        Arrays.sort(keys);
        matchers = new TuplesMatcher[keys.length];
        for (String key : keys)
            if (tuples.get(key) == null)
                return;
        matchers[0] = new AllTuplesMatcher(sharesSizes.length,
                sources.get(keys[0]), tuples.get(keys[0]));
        Integer[] currentSourceColumns = matchers[0].getResultColumns();
        for (int i = 1; i < keys.length; i++) {
            matchers[i] = new PartialMatchTuplesMatcher(sharesSizes.length,
                    sources.get(keys[i]), tuples.get(keys[i]),
                    currentSourceColumns);
            currentSourceColumns = matchers[i].getResultColumns();
        }
        recEmit(0, null);
    }
}
