package joiner.hadoop;

import java.util.Map;

public class ConfiguredForTask {
    protected Integer[] sharesSizes;
    protected Map<String, Integer[]> sources;

    protected ConfiguredForTask(TaskConfiguration conf) {
        sharesSizes = conf.sharesSizes;
        sources = conf.getSources();
    }
}
