package joiner.hadoop;

import joiner.stuff.Utils;

import java.util.*;

public class PartialMatchTuplesMatcher extends TuplesMatcher {

    private Integer[] sourceColumns;

    private Integer[] matchedColumns;
    private Integer[] newColumns;

    private ArrayList<Integer[]> content;

    private Integer[] sourceTuple;

    private Integer[] currentToKey;
    private Integer[] currentToValue;

    public static class IntegerArrayComparator implements Comparator<Integer[]> {
        @Override
        public int compare(Integer[] l, Integer[] r) {
            int minsize = (l.length < r.length) ? l.length : r.length;
            for (int i = 0; i < minsize; i++) {
                int v = l[i].compareTo(r[i]);
                if (v != 0) return v;
            }
            return Integer.compare(l.length, r.length);
        }
    }

    private Map<Integer[], List<Integer[]>> matchMap = new TreeMap<>(new IntegerArrayComparator());

    private void prepareOffsets() {
        /*
		 * Kolumny zawarte w tuplach reprezentowanych przez bieżący obiekt
		 * dzielą się na: - newColumns - dodawane, - matchedColumns -
		 * sprawdzane, czy pasują do aktualnie tworzonego tupla.
		 */
        Set<Integer> sourceColumnsSet = new HashSet<>();
        for (Integer v : sourceColumns)
            sourceColumnsSet.add(v);
        ArrayList<Integer> matchedColumnsList = new ArrayList<>();
        ArrayList<Integer> newColumnsList = new ArrayList<>();
        for (Integer v : currentColumns) {
            if (sourceColumnsSet.contains(v))
                matchedColumnsList.add(v);
            else
                newColumnsList.add(v);
        }
        matchedColumns = matchedColumnsList.toArray(new Integer[0]);
        newColumns = newColumnsList.toArray(new Integer[0]);

        currentToKey = new Integer[matchedColumns.length];
        for (int i = 0; i < matchedColumns.length; i++) {
            for (int j = 0; j < currentColumns.length; j++) {
                if (matchedColumns[i].equals(currentColumns[j])) {
                    currentToKey[i] = j;
                    break;
                }
            }
        }
        currentToValue = new Integer[newColumns.length];
        for (int i = 0; i < newColumns.length; i++) {
            for (int j = 0; j < currentColumns.length; j++) {
                if (newColumns[i].equals(currentColumns[j])) {
                    currentToValue[i] = j;
                    break;
                }
            }
        }
    }

    private Integer[] genericSubTuple(Integer[] tuple, Integer[] indices) {
        Integer[] result = new Integer[indices.length];
        for (int i = 0; i < indices.length; i++)
            result[i] = tuple[indices[i]];
        return result;
    }

    private Integer[] currentTupleToKey(Integer[] tuple) {
        return genericSubTuple(tuple, currentToKey);
    }

    private Integer[] currentTupleToValue(Integer[] tuple) {
        return genericSubTuple(tuple, currentToValue);
    }

    private void prepareMatchMap() {
        for (Integer[] tuple : content)
            Utils.addToMapWithLists(matchMap, currentTupleToKey(tuple),
                    currentTupleToValue(tuple));
    }

    public PartialMatchTuplesMatcher(int columnsNumber,
                                     Integer[] currentColumns, ArrayList<Integer[]> content,
                                     Integer[] sourceColumns) {
        super(columnsNumber, currentColumns);
        this.sourceColumns = sourceColumns;
        this.content = content;
        prepareOffsets();
        prepareMatchMap();
    }

    public class PartialMatchIterator implements Iterator<Integer[]> {
        private Integer[] baseTuple;
        private Integer[] newColumns;
        private List<Integer[]> newTuples;

        private Iterator<Integer[]> internalIterator;

        public PartialMatchIterator(Integer[] baseTuple, Integer[] newColumns,
                                    List<Integer[]> newTuples) {
            this.baseTuple = baseTuple;
            this.newColumns = newColumns;
            this.newTuples = newTuples;
            if (newTuples != null)
                internalIterator = newTuples.iterator();
        }

        @Override
        public boolean hasNext() {
            if (newTuples == null)
                return false;
            return internalIterator.hasNext();
        }

        @Override
        public Integer[] next() {
            Integer[] tuple = internalIterator.next();
            Integer[] resultTuple = baseTuple.clone();
            for (int i = 0; i < newColumns.length; i++)
                resultTuple[newColumns[i]] = tuple[i];
            return resultTuple;
        }

        @Override
        public void remove() {
        }

    }

    @Override
    public Iterator<Integer[]> iterator() {
        Integer[] key = new Integer[matchedColumns.length];
        for (int i = 0; i < matchedColumns.length; i++)
            key[i] = sourceTuple[matchedColumns[i]];
        List<Integer[]> values = matchMap.get(key);
        return new PartialMatchIterator(sourceTuple, newColumns, values);
    }

    @Override
    public void match(Integer[] sourceTuple) {
        this.sourceTuple = sourceTuple;
    }

    @Override
    public Integer[] getResultColumns() {
        Integer[] all = new Integer[sourceColumns.length + newColumns.length];
        for (int i = 0; i < sourceColumns.length; i++)
            all[i] = sourceColumns[i];
        for (int i = 0; i < newColumns.length; i++)
            all[sourceColumns.length + i] = newColumns[i];
        Arrays.sort(all);
        return all;
    }

}
