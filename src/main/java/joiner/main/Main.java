package joiner.main;

import joiner.hadoop.JoinTask;
import joiner.solver.JoinProblemInstance;
import joiner.solver.JoinSolver;
import joiner.solver.OctaveEquationsSolver;
import joiner.solver.OctaveSession;
import joiner.stuff.MyLogger;
import joiner.stuff.Utils;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.util.*;

public class Main {
    public static boolean REDUCERS_LOGGING = false;
    public static boolean SKIP_HADOOP = false;

    private static MyLogger logger = new MyLogger(Main.class);

    private String configFile;
    private String saveSharesFile;
    private String loadSharesFile;
    private String[] commandLineArgs;

    private int maxReducers;
    private String outputFile;
    private ArrayList<String> inputFiles = new ArrayList<>();
    private ArrayList<Integer> relationsSizes = new ArrayList<>();
    private ArrayList<String[]> relationsSchemas = new ArrayList<>();

    private ArrayList<Integer> shares;

    String[] allColumns;

    public Main(String configFile, String saveSharesFile, String loadSharesFile, String[] args) {
        this.configFile = configFile;
        this.saveSharesFile = saveSharesFile;
        this.loadSharesFile = loadSharesFile;
        commandLineArgs = args;
    }

    private boolean readConfigFile() {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(configFile));
            String line;
            if ((line = br.readLine()) == null)
                return false;
            String[] tokens = StringUtils.split(line);
            if (tokens.length != 2)
                return false;
            maxReducers = Integer.parseInt(tokens[0]);
            outputFile = tokens[1];
            while ((line = br.readLine()) != null) {
                tokens = StringUtils.split(line);
                if (tokens.length == 0)
                    continue;
                if (tokens.length < 4)
                    return false;
                inputFiles.add(tokens[0]);
                relationsSizes.add(new Integer(tokens[1]));
                String[] columns = new String[tokens.length - 2];
                System.arraycopy(tokens, 2, columns, 0, tokens.length - 2);
                relationsSchemas.add(columns);
            }
            if (inputFiles.size() < 2)
                return false;
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void storeShares() {
        logger.log("Saving computed shares' values to " + saveSharesFile + ".");
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(saveSharesFile);
            for (int i = 0; i < allColumns.length; i++) {
                writer.println(allColumns[i] + " " + shares.get(i));
            }
        } catch (FileNotFoundException e) {
            System.err.println("Error while saving shares. [NOT FATAL]");
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
    }

    private boolean loadShares() {
        logger.log("Loading computed shares' values from " + loadSharesFile + ".");
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(loadSharesFile));

            int counter = 0;
            String line;
            shares = new ArrayList<>();
            while (counter < allColumns.length && (line = reader.readLine()) != null) {
                String[] tokens = StringUtils.split(line);
                if (tokens.length != 2) {
                    if (line.isEmpty()) {
                        continue;
                    } else {
                        System.err.println("Error while loading shares: Unexpected line: " + line);
                        return false;
                    }
                }

                String columnId = tokens[0];
                int shareValue = Integer.valueOf(tokens[1]);
                if (!columnId.equals(allColumns[counter])) {
                    System.err.println("Error while loading shares: Unexpected column ID: " + columnId);
                    return false;
                }

                shares.add(shareValue);
                counter++;
            }

            if (shares.size() < allColumns.length) {
                System.err.println("Error while loading shares: Not enough values found.");
                return false;
            }

            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void showConfigDetails() {
        logger.log("Configuration details:");
        logger.log("   Max reducers: " + maxReducers);
        logger.log("   Output file: " + outputFile);
        logger.log("   Input files (" + inputFiles.size() + "):");
        for (int i = 0; i < inputFiles.size(); i++) {
            logger.log("      File " + (i + 1) + ": "
                    + inputFiles.get(i));
            logger.log("         Size: " + relationsSizes.get(i));
            logger.log("         Columns: ["
                    + StringUtils.join(relationsSchemas.get(i), ", ") + "]");
        }
    }

    private void computeShares() {
        logger.log("Computing shares' sizes...");

        OctaveSession octaveSession = new OctaveSession();
        JoinSolver solver = new JoinSolver(maxReducers, octaveSession);
        for (int i = 0; i < inputFiles.size(); i++) {
            solver.addRelation(relationsSizes.get(i), relationsSchemas.get(i));
        }

        Map<String, Integer> sharesMap = solver.solve();
        octaveSession.close();
        if (sharesMap == null) {
            System.err.println("Shares not found. Cannot continue.");
            System.exit(1);
        }
        shares = new ArrayList<>();
        for (String col : allColumns) {
            shares.add(sharesMap.get(col));
        }
    }

    private void collectColumns() {
        List<String> columnsTmp = new ArrayList<>();
        for (String[] cols : relationsSchemas) {
            Collections.addAll(columnsTmp, cols);
        }
        allColumns = Utils.sortAndUnique(columnsTmp.toArray(new String[columnsTmp.size()]), new String[0]);
    }

    private int getColumnNr(String col) {
        for (int i = 0; i < allColumns.length; i++)
            if (allColumns[i].equals(col))
                return i;
        return -1;
    }

    private void runHadoopJob() throws Exception {
        logger.log("Running hadoop task...");

        JoinTask task = new JoinTask();
        task.setCommandLineArgs(commandLineArgs);
        task.setOutputFile(outputFile);
        task.setReducersNumber(maxReducers);
        task.setSharesSizes(shares.toArray(new Integer[shares.size()]));
        Map<String, Integer[]> sources = new HashMap<>();
        for (int i = 0; i < inputFiles.size(); i++) {
            List<Integer> columnsNrs = new ArrayList<>();
            for (String column : relationsSchemas.get(i))
                columnsNrs.add(getColumnNr(column));
            sources.put(inputFiles.get(i), columnsNrs.toArray(new Integer[0]));
        }
        task.setSources(sources);

        task.launch();
    }

    public void run() throws Exception {
        // Initialization
        if (!readConfigFile()) {
            System.err.println("Error while reading config file.");
            System.exit(1);
        }
        if (MyLogger.isLoggedClass(Main.class))
            showConfigDetails();

        // Computing stuff
        collectColumns();
        if (loadSharesFile != null) {
            if (!loadShares()) {
                shares = null;
            }
        }
        if (shares == null || shares.isEmpty()) {
            computeShares();
        }
        if (saveSharesFile != null) {
            storeShares();
        }

        if (logger.isLogged()) {
            logger.log("Shares' values:");
            for (int i = 0; i < allColumns.length; i++)
                logger.log("   " + allColumns[i] + ": " + shares.get(i));

        }

        // Actual hadoop
        if (!SKIP_HADOOP) {
            runHadoopJob();
        }
    }

    public static void main(String[] args) throws Exception {
        String configFile = null;
        String saveSharesFile = null;
        String loadSharesFile = null;
        List<String> remainingArgs = new ArrayList<>();
        for (int i = 0; i < args.length; i++) {
            String a = args[i];
            switch (a) {
                case "--main_logging":
                    MyLogger.addClassToLog(Main.class);
                    break;
                case "--octave_logging":
                    MyLogger.addClassToLog(OctaveEquationsSolver.class);
                    break;
                case "--structure_logging":
                    MyLogger.addClassToLog(JoinSolver.class);
                    MyLogger.addClassToLog(JoinProblemInstance.class);
                    break;
                case "--hadoop_logging":
                    MyLogger.addClassToLog(JoinTask.class);
                    break;
                case "--reducers_logging":
                    REDUCERS_LOGGING = true;
                    break;
                case "--skip_hadoop":
                    SKIP_HADOOP = true;
                    break;
                case "--save_shares_file":
                    if (i + 1 < args.length) {
                        saveSharesFile = args[i+1];
                        i++;
                    }
                    break;
                case "--load_shares_file":
                    if (i + 1 < args.length) {
                        loadSharesFile = args[i+1];
                        i++;
                    }
                    break;
                default:
                    if (configFile == null && a.charAt(0) != '-') {
                        configFile = a;
                    } else {
                        remainingArgs.add(a);
                    }
                    break;
            }
        }
        if (configFile == null) {
            System.err
                    .println("You have to pass config file as first argument.");
            System.exit(1);
        }
        new Main(configFile,
                 saveSharesFile,
                 loadSharesFile,
                 remainingArgs.toArray(new String[remainingArgs.size()])).run();
    }

}
