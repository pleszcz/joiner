package joiner.research;

import dk.ange.octave.OctaveEngine;
import dk.ange.octave.OctaveEngineFactory;
import dk.ange.octave.type.OctaveDouble;

public class MultiThreadedOctaveTest {
    private static class OctaveExecutor implements Runnable {
        private String cmd;
        private Double result;
        private OctaveEngine engine;

        private OctaveExecutor(String cmd) {
            this.cmd = cmd;
        }

        public Double getResult() {
            return result;
        }

        public OctaveEngine getEngine() {
            return engine;
        }

        @Override
        public void run() {
            engine = new OctaveEngineFactory().getScriptEngine();
            engine.eval(cmd);
            result = ((OctaveDouble) engine.get("x")).get(1);
            engine.close();
        }
    }

    public static void main(String[] args) {
        Thread ts[] = new Thread[4];
        OctaveExecutor os[] = new OctaveExecutor[4];

        ts[0] = new Thread(os[0] = new OctaveExecutor("x = 5;"));
        ts[1] = new Thread(os[1] = new OctaveExecutor("while true; x = 6; end"));
        ts[2] = new Thread(os[2] = new OctaveExecutor("x = 7;"));
        ts[3] = new Thread(os[3] = new OctaveExecutor("while true; x = 8; end"));

        try {
            Thread.sleep(500, 0);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < 4; i++) {
            if (ts[i].isAlive()) {
                System.out.println("Killing " + i);
                os[i].getEngine().destroy();
                System.out.println("Joining " + i);
                try {
                    ts[i].join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println(i + ". result: " + os[i].getResult());
            }
        }

    }
}
