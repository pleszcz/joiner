package joiner.stuff;

import java.util.*;

public class Utils {
    public static <K, V> void addToMapWithLists(Map<K, List<V>> map, K key, V value) {
        List<V> list = map.get(key);
        if (list == null) {
            list = new ArrayList<>();
            list.add(value);
            map.put(key, list);
        } else {
            list.add(value);
        }
    }

    public static <T> T[] sortAndUnique(T[] in, T[] out) {
        Arrays.sort(in);
        int m = 0;
        for (int i = 0; i < in.length; i++) {
            if (m == 0 || !in[m - 1].equals(in[i])) {
                in[m++] = in[i];
            }
        }
        List<T> outList = new ArrayList<>();
        for (int i = 0; i < m; i++)
            outList.add(in[i]);
        return outList.toArray(out);
    }

    public static <T extends Comparable<T>> List<T> collectionToSortedList(Collection<T> col) {
        List<T> result = new ArrayList<>(col);
        Collections.sort(result);
        return result;
    }
}
