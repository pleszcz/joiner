package joiner.stuff;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by piotrek on 23.01.15.
 */
public class MyLogger {
    private static Set<String> loggedClassesNames = new HashSet<>();

    private String currentClassName;
    private String currentSimpleName;

    public static void addClassToLog(Class cl) {
        loggedClassesNames.add(cl.getCanonicalName());
    }

    public MyLogger(Class cl) {
        currentClassName = cl.getCanonicalName();
        currentSimpleName = cl.getSimpleName();
    }

    public void log(String msg) {
        if (loggedClassesNames.contains(currentClassName)) {
            System.out.println("{" + currentSimpleName + "} " + msg);
        }
    }

    public static boolean isLoggedClass(Class cl) {
        return loggedClassesNames.contains(cl.getCanonicalName());
    }

    public boolean isLogged() {
        return loggedClassesNames.contains(currentClassName);
    }
}
