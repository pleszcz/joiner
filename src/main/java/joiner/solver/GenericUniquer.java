package joiner.solver;

import joiner.stuff.Utils;

import java.util.*;

public class GenericUniquer<T extends Comparable<T>> {
    private Set<T> values = new HashSet<>();
    private Map<T, Integer> map;
    private List<T> sortedValues;

    public void add(T s) {
        values.add(s);
    }

    public void computeIndices(int first) {
        int i = first;
        map = new HashMap<>();
        for (T s : getAllSortedValues()) {
            map.put(s, i);
            i++;
        }
    }

    public int size() {
        return values.size();
    }

    public int getIndex(T s) {
        return map.get(s);
    }

    public T getValue(int x) {
        return getAllSortedValues().get(x);
    }

    public List<Integer> getSortedIndices(Collection<T> ts) {
        List<Integer> result = new ArrayList<>();
        for (T t : ts) {
            result.add(getIndex(t));
        }
        Collections.sort(result);
        return result;
    }

    public List<T> getSortedValues(Collection<Integer> is) {
        List<T> result = new ArrayList<>();
        for (Integer i : is) {
            result.add(getValue(i));
        }
        Collections.sort(result);
        return result;
    }

    public List<T> getAllSortedValues() {
        if (sortedValues == null) {
            sortedValues = Utils.collectionToSortedList(values);
        }
        return sortedValues;
    }
}
