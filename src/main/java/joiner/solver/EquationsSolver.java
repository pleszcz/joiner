package joiner.solver;

import java.util.ArrayList;
import java.util.List;

abstract public class EquationsSolver {
    protected final double precision = 0.00001;

    protected int sharesNumber;
    protected int reducersNumber;

    protected List<Integer[][]> equations = new ArrayList<>();

    public EquationsSolver(int sharesNumber, int reducersNumber) {
        this.sharesNumber = sharesNumber;
        this.reducersNumber = reducersNumber;
    }

    public void addEquation(Integer[][] equation) {
        equations.add(equation);
    }

    abstract public Double[] solve();
}
