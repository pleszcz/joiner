package joiner.solver;

import joiner.stuff.MyLogger;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

public class JoinProblemInstance<T extends Comparable<T>> {
    private static MyLogger logger = new MyLogger(JoinProblemInstance.class);

    private OctaveSession octaveSession;

    private final int reducersNumber;

    private GenericUniquer<T> mapper = new GenericUniquer<>();
    private Double[] realShares;
    private Integer[] shares;

    private List<Relation> relations = new ArrayList<>();
    private double bestCost = Double.NaN;

    private boolean initialized = false;

    private class Relation {
        public int size;
        public List<T> columns;
        public Set<Integer> variables;

        public Relation(int size, T[] columns) {
            this(size, Arrays.asList(columns));
        }

        public Relation(int size, Collection<T> columns) {
            this.size = size;
            this.columns = new ArrayList<>(columns);
        }

        public void computeVariables() {
            Set<Integer> vars = new HashSet<>();
            for (T col : columns) {
                vars.add(mapper.getIndex(col));
            }
            variables = new HashSet<>();
            for (int i = 0; i < mapper.size(); i++) {
                if (!vars.contains(i)) {
                    variables.add(i);
                }
            }
        }

        public List<T> remainingColumns(Set<T> columnsToDeduct) {
            List<T> result = new ArrayList<>();
            for (T t : columns) {
                if (!columnsToDeduct.contains(t)) {
                    result.add(t);
                }
            }
            return result;
        }

        @Override
        public String toString() {
            return "(" + StringUtils.join(columns, ", ") + ")";
        }
    }

    private class SubProblemGenerator implements Iterator<JoinProblemInstance<T>> {
        private int currentColumnIndex;

        public SubProblemGenerator() {
            currentColumnIndex = 0;
            if (mapper.size() <= 1)
                currentColumnIndex = 1;
        }

        @Override
        public boolean hasNext() {
            return currentColumnIndex < mapper.size();
        }

        @Override
        public JoinProblemInstance<T> next() {
            if (!hasNext()) throw new NoSuchElementException();
            JoinProblemInstance<T> result = makeProblemWithoutColumn(mapper.getValue(currentColumnIndex));
            currentColumnIndex++;
            return result;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    public JoinProblemInstance(int reducersNumber, OctaveSession octaveSession) {
        this.reducersNumber = reducersNumber;
        this.octaveSession = octaveSession;
    }

    @SafeVarargs
    public final void addRelation(int size, T... columns) {
        addRelation(size, Arrays.asList(columns));
    }

    public void addRelation(int size, Collection<T> columns) {
        relations.add(new Relation(size, columns));
        for (T column : columns) {
            mapper.add(column);
        }
    }

    public void initializeSolver() {
        if (!initialized) {
            mapper.computeIndices(0);
            for (Relation relation : relations) {
                relation.computeVariables();
            }
            logProblemMsg("initialized");
//            if (logger.isLogged()) {
//                List<String> relationsStrings = new ArrayList<>();
//                for (Relation r : relations) {
//                    relationsStrings.add(r.toString());
//                }
//                logProblemMsg("relations: " + StringUtils.join(relationsStrings, "; "));
//            }
            initialized = true;
        }
    }

    private void logProblemMsg(String extraMsg) {
        if (MyLogger.isLoggedClass(JoinProblemInstance.class)) {
            logger.log("[" + StringUtils.join(mapper.getAllSortedValues(), ", ") + "] " + extraMsg);
        }
    }

    private Set<Integer> findDominatedShares() {
        Set<Integer> dominatedColumns = new HashSet<>();

        int occurrences[] = new int[mapper.size()];
        int pairOccurrences[][] = new int[mapper.size()][mapper.size()];
        Arrays.fill(occurrences, 0);
        for (int[] pairOccurrence : pairOccurrences) {
            Arrays.fill(pairOccurrence, 0);
        }

        // counting occurrences
        for (Relation relation : relations) {
            for (int i = 0; i < relation.columns.size(); i++) {
                int iNum = mapper.getIndex(relation.columns.get(i));
                occurrences[iNum]++;
                for (int j = i + 1; j < relation.columns.size(); j++) {
                    int jNum = mapper.getIndex(relation.columns.get(j));
                    pairOccurrences[iNum][jNum]++;
                    pairOccurrences[jNum][iNum]++;
                }
            }
        }

        // finding dominated shares
        for (int i = 0; i < mapper.size(); i++) {
            if (dominatedColumns.contains(i)) {
                continue;
            }
            for (int j = 0; j < mapper.size(); j++) {
                if (i == j) {
                    continue;
                }
                if (pairOccurrences[i][j] == occurrences[i] && !dominatedColumns.contains(j)) {
                    dominatedColumns.add(i);
                    break;
                }
            }
        }

        return (dominatedColumns.size() == 0) ? null : dominatedColumns;
    }

    public JoinProblemInstance<T> makeProblemWithoutDominatedColumns() {
        Set<Integer> dominatedColumnsIndices = findDominatedShares();
        if (dominatedColumnsIndices == null) {
            return null;
        }

        List<T> dominatedColumns = mapper.getSortedValues(dominatedColumnsIndices);
        logProblemMsg("generating sub-problem without the following dominated columns: " +
                      StringUtils.join(dominatedColumns, ", "));
        return makeProblemWithoutColumns(new HashSet<>(dominatedColumns));
    }

    private List<T> findColumnsWithSmallShares() {
        List<Integer> smallSharesIndices = new ArrayList<>();
        for (int i = 0; i < mapper.size(); i++)
            if (realShares[i] < 1.0)
                smallSharesIndices.add(i);
        return mapper.getSortedValues(smallSharesIndices);
    }

    public JoinProblemInstance<T> makeProblemWithoutSmallShares() {
        List<T> columnsWithSmallShares = findColumnsWithSmallShares();
        if (columnsWithSmallShares.size() == 0)
            return null;
        logProblemMsg("generating sub-problem without columns having small shares: " +
                StringUtils.join(columnsWithSmallShares, ", "));
        return makeProblemWithoutColumns(new HashSet<>(columnsWithSmallShares));
    }

    private JoinProblemInstance<T> makeProblemWithoutColumns(Set<T> columnsToRemove) {
        resetSolutionState();

        Set<Integer> columnsToRemoveIndices = new HashSet<>(mapper.getSortedIndices(columnsToRemove));
        realShares = new Double[mapper.size()];
        for (int i = 0; i < mapper.size(); i++) {
            if (columnsToRemoveIndices.contains(i)) {
                realShares[i] = 1.0;
            }
        }

        JoinProblemInstance<T> subProblem = new JoinProblemInstance<>(reducersNumber, octaveSession);
        for (Relation relation : relations) {
            List<T> columns = relation.remainingColumns(columnsToRemove);
            if (columns.size() > 0) {
                subProblem.addRelation(relation.size, columns);
            }
        }
        return subProblem;
    }

    private JoinProblemInstance<T> makeProblemWithoutColumn(T columnToRemove) {
        Set<T> singleton = new HashSet<>();
        singleton.add(columnToRemove);
        logProblemMsg("generating sub-problem with deducted column: " + columnToRemove);
        return makeProblemWithoutColumns(singleton);
    }

    public void adaptToSubSolution(Map<T, Double> realSubSolution) {
        logProblemMsg("adapting to solution from sub-problem!");
        for (Map.Entry<T, Double> column : realSubSolution.entrySet()) {
            realShares[mapper.getIndex(column.getKey())] = column.getValue();
        }
    }

    public Iterable<JoinProblemInstance<T>> subProblems() {
        return new Iterable<JoinProblemInstance<T>>() {
            @Override
            public Iterator<JoinProblemInstance<T>> iterator() {
                return new SubProblemGenerator();
            }
        };
    }

    private <S> Map<T, S> getGenericSolution(S[] base) {
        Map<T, S> result = new HashMap<>();
        for (T column : mapper.getAllSortedValues()) {
            result.put(column, base[mapper.getIndex(column)]);
        }
        return result;
    }

    public Map<T, Double> getRealSolution() {
        return getGenericSolution(realShares);
    }

    private Integer[][] makeEquation(int column) {
        List<Integer[]> equation = new ArrayList<>();
        for (Relation relation : relations) {
            List<Integer> product = new ArrayList<>();
            if (!relation.variables.contains(column)) {
                continue;
            }
            product.add(relation.size);
            product.addAll(relation.variables);
            equation.add(product.toArray(new Integer[product.size()]));
        }
        return equation.toArray(new Integer[equation.size()][]);
    }

    public boolean solve() {
        logProblemMsg("solving...");

        resetSolutionState();

        EquationsSolver solver = new OctaveEquationsSolver(mapper.size(), reducersNumber, octaveSession);
        for (int column = 0; column < mapper.size(); column++) {
            solver.addEquation(makeEquation(column));
        }
        realShares = solver.solve();

        if (realShares != null) {
            logProblemMsg("solution found!");
        } else {
            logProblemMsg("solution not found!");
        }

        return realShares != null;
    }

    private ArrayList<Integer> generateSolution(int mask, ArrayList<Integer> activeShares) {
        ArrayList<Integer> result = new ArrayList<>();
        for (int i = 0; i < activeShares.size(); i++) {
            double realValue = realShares[activeShares.get(i)];
            if ((mask & (1 << i)) > 0) {
                realValue = Math.ceil(realValue);
            } else {
                realValue = Math.floor(realValue);
            }
            result.add(((Double) realValue).intValue());
        }
        return result;
    }

    private double computeSolutionCost(ArrayList<Integer> solution, ArrayList<Integer> activeShares) {
        for (int i = 0; i < solution.size(); i++) {
            shares[activeShares.get(i)] = solution.get(i);
        }

        double finalValue = 0;
        for (Relation relation : relations) {
            double componentValue = relation.size;
            for (int var : relation.variables) {
                componentValue *= shares[var];
            }
            finalValue += componentValue;
        }

        double K = 1.0;
        for (int v : solution) {
            K *= v;
        }

        return finalValue / K;
    }

    private boolean isCorrectSolution(ArrayList<Integer> solution) {
        int K = 1;
        for (int v : solution) {
            K *= v;
        }
        return K <= reducersNumber;
    }

    private void prepareIntegerShares() {
        shares = new Integer[realShares.length];
        ArrayList<Integer> activeShares = new ArrayList<>();
        for (int i = 0; i < realShares.length; i++) {
            if (realShares[i] <= 1.0) {
                shares[i] = 1;
            } else {
                activeShares.add(i);
            }
        }

        int maxBitMask = 1 << activeShares.size();
        ArrayList<Integer> bestSolution = generateSolution(0, activeShares);
        bestCost = computeSolutionCost(bestSolution, activeShares);
        for (int bitMask = 1; bitMask < maxBitMask; bitMask++) {
            ArrayList<Integer> solution = generateSolution(bitMask, activeShares);
            if (isCorrectSolution(solution)) {
                double cost = computeSolutionCost(solution, activeShares);
                if (cost < bestCost) {
                    bestCost = cost;
                    bestSolution = solution;
                }
            }
        }

        for (int i = 0; i < bestSolution.size(); i++) {
            shares[activeShares.get(i)] = bestSolution.get(i);
        }
    }

    public Map<T, Integer> getIntegerSolution() {
        if (shares == null) {
            prepareIntegerShares();
        }
        return getGenericSolution(shares);
    }

    public double getCurrentSolutionCost() {
        if (shares == null) {
            prepareIntegerShares();
        }
        return bestCost;
    }

    public String key() {
        return StringUtils.join(mapper.getAllSortedValues(), ",");
    }

    private void resetSolutionState() {
        shares = null;
        bestCost = Double.NaN;
        realShares = null;
    }
}
