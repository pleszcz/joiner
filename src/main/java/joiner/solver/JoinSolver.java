package joiner.solver;

import joiner.stuff.MyLogger;
import org.apache.hadoop.util.StringUtils;

import java.util.*;

public class JoinSolver {
    private static MyLogger logger = new MyLogger(JoinSolver.class);

    private JoinProblemInstance<String> mainProblem;

    private class JoinSolution implements Comparable<JoinSolution> {
        public Map<String, Integer> integerSolution;
        public Map<String, Double> realSolution;
        public double cost;

        private JoinSolution(Map<String, Integer> integerSolution, Map<String, Double> realSolution, double cost) {
            this.integerSolution = integerSolution;
            this.realSolution = realSolution;
            this.cost = cost;
        }

        @Override
        public int compareTo(JoinSolution joinSolution) {
            return Double.compare(cost, joinSolution.cost);
        }

        @Override
        public String toString() {
            List<String> pairs = new ArrayList<>();
            for (Map.Entry<String, Integer> it : integerSolution.entrySet()) {
                pairs.add("(" + it.getKey() + ":" + it.getValue() + ")");
            }
            Collections.sort(pairs);
            return "JoinSolution{" +
                    "integerSolution=[" + StringUtils.join(", ", pairs) +
                    "], cost=" + cost +
                    '}';
        }
    }

    private Map<String, JoinSolution> foundSolutions = new HashMap<>();

    public JoinSolver(int reducersNumber, OctaveSession octaveSession) {
        mainProblem = new JoinProblemInstance<>(reducersNumber, octaveSession);
    }

    public void addRelation(int size, String... columns) {
        mainProblem.addRelation(size, columns);
    }

    private <S> void printSolution(Map<String, S> solution) {
        List<String> lines = new ArrayList<>();
        for (Map.Entry<String, S> v : solution.entrySet()) {
            lines.add("   " + v.getKey() + " = " + v.getValue());
        }
        String[] linesArray = lines.toArray(new String[lines.size()]);
        Arrays.sort(linesArray);
        for (String line : linesArray) {
            if (!logger.isLogged()) {
                System.out.println(line);
            } else {
                logger.log(line);
            }
        }
    }

    public Map<String, Integer> solve() {
        JoinSolution solution = internalSolveWithMemorization(mainProblem);
        if (solution != null) {
            if (MyLogger.isLoggedClass(JoinSolver.class)) {
                logger.log("Real solution:");
                printSolution(solution.realSolution);
                logger.log("Integer solution:");
            }
            printSolution(solution.integerSolution);
        }
        return solution.integerSolution;
    }

    private JoinSolution internalSolve(JoinProblemInstance<String> problemInstance) {
        problemInstance.initializeSolver();

        // Trying to solve sub-problem that is the result of removing dominated columns.
        {
            JoinProblemInstance<String> subProblem = problemInstance.makeProblemWithoutDominatedColumns();
            if (subProblem != null) {
                if (handleSubproblem(problemInstance, subProblem)) {
                    return makeJoinSolution(problemInstance);
                } else {
                    return null;
                }
            }
        }

        // Trying to solve current problem (by solving the equation system in Octave).
        if (problemInstance.solve()) {
            JoinProblemInstance<String> subProblem = problemInstance.makeProblemWithoutSmallShares();
            if (subProblem != null) {
                if (handleSubproblem(problemInstance, subProblem)) {
                    return makeJoinSolution(problemInstance);
                } else {
                    return null;
                }
            }
            return makeJoinSolution(problemInstance);
        }

        // Trying to reduce the current problem to n sub-problems (for each column) - each sub-problem is the current
        // problem with particular column removed. The best solution of sub-problems is chosen and adapted to the
        // current problem.
        JoinSolution bestSolution = null;
        for (JoinProblemInstance<String> subProblem : problemInstance.subProblems()) {
            if (handleSubproblem(problemInstance, subProblem)) {
                JoinSolution currentSolution = makeJoinSolution(problemInstance);
                if (bestSolution == null || currentSolution.compareTo(bestSolution) < 0) {
                    bestSolution = currentSolution;
                }
            }
        }
        
        return bestSolution;
    }

    private boolean handleSubproblem(JoinProblemInstance<String> parentProblem,
                                     JoinProblemInstance<String> childProblem) {
        childProblem.initializeSolver();
        JoinSolution solution = internalSolveWithMemorization(childProblem);
        if (solution != null) {
            parentProblem.adaptToSubSolution(solution.realSolution);
            return true;
        }
        return false;
    }

    private JoinSolution internalSolveWithMemorization(JoinProblemInstance<String> problemInstance) {
        problemInstance.initializeSolver();
        String key = problemInstance.key();

        JoinSolution solution = foundSolutions.get(key);
        if (solution != null) {
            logger.log("memorized solution: " + solution);
            return solution;
        }

//        logger.log("no saved solution for: " + key);
        solution = internalSolve(problemInstance);
        if (solution != null) {
//            logger.log("saving solution for: " + key);
            foundSolutions.put(key, solution);
        }

        logger.log("newly computed solution: " + solution);
        return solution;
    }

    private JoinSolution makeJoinSolution(JoinProblemInstance<String> problemInstance) {
        return new JoinSolution(
                problemInstance.getIntegerSolution(),
                problemInstance.getRealSolution(),
                problemInstance.getCurrentSolutionCost());
    }
}
