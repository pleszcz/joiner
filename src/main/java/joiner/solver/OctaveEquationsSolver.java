package joiner.solver;

import dk.ange.octave.OctaveEngine;
import dk.ange.octave.type.OctaveBoolean;
import dk.ange.octave.type.OctaveDouble;
import joiner.stuff.MyLogger;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class OctaveEquationsSolver extends EquationsSolver {
    private static MyLogger logger = new MyLogger(OctaveEquationsSolver.class);

    final static private double EPS = 1e-6;

    static public boolean USE_JACOBIAN = false;

    final static private int MC_ITERATIONS = 75;
    final static private int FSOLVE_ITERATIONS = 2000;

    private OctaveEngine octave;

    public OctaveEquationsSolver(int sharesNumber, int reducersNumber, OctaveSession octaveSession) {
        super(sharesNumber, reducersNumber);
        this.octave = octaveSession.getOctaveEngine();
    }

    private boolean hasVariable(Integer[] components, Integer variable) {
        for (int i = 1; i < components.length; i++)
            if (variable.equals(components[i] + 1))
                return true;
        return false;
    }

    private List<String> makeProduct(Integer[] components, Integer differentiateBy) {
        if (differentiateBy != null && !hasVariable(components, differentiateBy))
            return null;
        List<String> container = new ArrayList<>();
        container.add(components[0].toString());
        for (int i = 1; i < components.length; i++) {
            Integer var = components[i] + 1;
            if (differentiateBy != null && var.equals(differentiateBy))
                continue;
            container.add("*x(" + var + ")");
        }
        return container;
    }

    private List<String> makeSum(Integer[][] components, Integer differentiateBy) {
        List<String> container = new ArrayList<>();
        for (Integer[] component : components) {
            List<String> product = makeProduct(component, differentiateBy);
            if (product != null && product.size() > 0) {
                if (container.size() > 0)
                    container.add(" + ");
                container.addAll(product);
            }
        }
        return container;
    }

    private List<String> makeYFormula(int equationNumber, Integer[][] equation) {
        List<String> container = new ArrayList<>();
        container.add("   y(" + equationNumber + ") = ");
        container.add(Integer.toString(reducersNumber) + "*x(" + (sharesNumber + 1) + ") - (");
        container.addAll(makeSum(equation, null));
        container.add(");\n");
        return container;
    }

    private List<String> makeYLimitFormula(int equationNumber) {
        List<String> container = new ArrayList<>();
        container.add("   y(" + equationNumber + ") = ");
        container.add(reducersNumber + " - ");
        container.add("x(1)");
        for (int i = 2; i <= sharesNumber; i++)
            container.add("*x(" + i + ")");
        container.add(";\n");
        return container;
    }

    private List<String> makeJacobianFormula(int equationNr, int varNr, Integer[][] equation) {
        List<String> container = new ArrayList<>();
        container.add("      jac(" + equationNr + ", " + varNr + ") = ");
        List<String> sum = makeSum(equation, varNr);
        if (sum == null || sum.size() == 0) {
            container.add("0;\n");
        } else {
            container.add("- (");
            container.addAll(sum);
            container.add(");\n");
        }
        return container;
    }

    private List<String> makeJacobianLimitFormula(int equationNr, int varNr) {
        List<String> container = new ArrayList<>();
        container.add("      jac(" + equationNr + ", " + varNr + ") = ");
        boolean first = true;
        for (int i = 1; i <= sharesNumber; i++) {
            if (i == varNr)
                continue;
            if (!first)
                container.add("*");
            container.add("x(" + i + ")");
            first = false;
        }
        container.add(";\n");
        return container;
    }

    private String makeFunctionDeclaration() {
        List<String> stringsContainer = new ArrayList<>();
        final int dimension = sharesNumber + 1;

        // header
        if (USE_JACOBIAN) {
            stringsContainer.add("function [y, jac] = f(x)\n");
        } else {
            stringsContainer.add("function [y] = f(x)\n");
        }

        // regular y(*)
        stringsContainer.add("   y = zeros(" + dimension + ", 1);\n");
        int equationCounter = 1;
        for (Integer[][] equation : equations) {
            stringsContainer.addAll(makeYFormula(equationCounter, equation));
            equationCounter++;
        }
        stringsContainer.addAll(makeYLimitFormula(equationCounter));

        if (USE_JACOBIAN) {
            // jacobian
            stringsContainer.add("   if (nargout == 2)\n");
            stringsContainer.add("      jac = zeros (" + dimension + ", " + dimension + ");\n");
            equationCounter = 1;
            for (Integer[][] equation : equations) {
                for (int varNr = 1; varNr <= sharesNumber; varNr++)
                    stringsContainer.addAll(makeJacobianFormula(equationCounter, varNr, equation));
                stringsContainer.add("      jac(" + equationCounter + ", " + dimension + ") = " + reducersNumber + ";\n");
                equationCounter++;
            }
            for (int varNr = 1; varNr <= sharesNumber; varNr++)
                stringsContainer.addAll(makeJacobianLimitFormula(dimension, varNr));
            stringsContainer.add("      jac(" + dimension + ", " + dimension + ") = 0;\n");
            stringsContainer.add("   endif\n");
        }

        // footer
        stringsContainer.add("endfunction");

        return StringUtils.join(stringsContainer, "");
    }

    private Double[] getComputedResult() {
        OctaveBoolean varFound = (OctaveBoolean) octave.get("found");
        OctaveDouble varX = (OctaveDouble) octave.get("x");
        OctaveDouble varInfo = (OctaveDouble) octave.get("info");
        OctaveDouble varFmax = (OctaveDouble) octave.get("fmax");

        if (logger.isLogged()) {
            logger.log("found: " + varFound.get(1));
            logger.log("info: " + Double.toString(varInfo.get(1)));
            logger.log("fmax: " + Double.toString(varFmax.get(1)));
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("x:");
            for (int i = 1; i <= sharesNumber + 1; i++)
                stringBuilder.append(" " + Double.toString(varX.get(i)));
            logger.log(stringBuilder.toString());
        }

        if (!varFound.get(1)) {
            return null;
        }

        Double[] result = new Double[sharesNumber];
        for (int i = 0; i < sharesNumber; i++)
            result[i] = varX.get(i + 1);

        return result;
    }

    private String makeCallSolverCommand() {
        return ("[found, x, fmax, info] = brute_solve(@f, " + reducersNumber +
                ", " + sharesNumber +
                ", " + MC_ITERATIONS +
                ", " + FSOLVE_ITERATIONS +
                ", " + EPS + ");");
    }

    private Double[] computeResult() {
        String functionDeclaration = makeFunctionDeclaration();
        logger.log("function declaration:");
        logger.log(functionDeclaration);
        octave.eval(functionDeclaration);

        String callSolverCommand = makeCallSolverCommand();
        logger.log("call solver command:");
        logger.log(callSolverCommand);
        octave.eval(callSolverCommand);

        return getComputedResult();
    }

    @Override
    public Double[] solve() {
        if (sharesNumber == 1) {
            return new Double[]{(double) reducersNumber};
        }
        Double[] result = computeResult();
        return result;
    }

}
