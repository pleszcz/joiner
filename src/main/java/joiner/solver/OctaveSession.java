package joiner.solver;

import dk.ange.octave.OctaveEngine;
import dk.ange.octave.OctaveEngineFactory;

public class OctaveSession {
    final static private String OCTAVE_SOLVE_ALGORITHM =
            "function [found, x, fmax, info, last_mc_iter, output, x0] = brute_solve(fun, k, dim, mc_iters, solve_iters, eps)\n" +
            "  found = false;\n" +
            "  root = k ^ (1/dim);\n" +
            "  last_mc_iter = -1;\n" +
            "\n" +
            "  function x00 = gen_x0()\n" +
            "    x00 = [];\n" +
            "    for i = 1:dim\n" +
            "      x00 = [x00; unifrnd(1, 5 * root)];\n" +
            "    end\n" +
            "    x00 = [x00; 0.1];\n" +
            "  end\n" +
            "\n" +
            "  for iter = 1:mc_iters\n" +
            "    x0 = gen_x0();\n" +
            "    [x, fval, info, output] = fsolve(fun, x0, optimset(\"MaxIter\", solve_iters));\n" +
            "    fmax = max(abs(fval));\n" +
            "    if (info == 1.0 || ((info == 2.0 || info == 3.0) && fmax < eps))\n" +
            "      found = true;\n" +
            "      last_mc_iter = iter;\n" +
            "      return;\n" +
            "    end\n" +
            "  end\n" +
            "endfunction\n";

    private OctaveEngine engine = null;

    public OctaveEngine getOctaveEngine() {
        if (engine == null) {
            engine = new OctaveEngineFactory().getScriptEngine();
            engine.eval(OCTAVE_SOLVE_ALGORITHM);
        }
        return engine;
    }

    public void close() {
        engine.close();
        engine = null;
    }
}
